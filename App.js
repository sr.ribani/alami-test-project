/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React from 'react';

import { NavigationContainer } from '@react-navigation/native';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import DeviceIdScreen from './DeviceId';
import HomeScreen from './Home';
import CartScreen from './Cart';
import ProgressbarScreen from './ProgressBar';

const Stack = createNativeStackNavigator()

const App = () => {

  return (
    <NavigationContainer>
      <Stack.Navigator>
        <Stack.Screen
          name="Home"
          component={HomeScreen}/>
        <Stack.Screen 
          name="Device Id"
          component={DeviceIdScreen}/>
        <Stack.Screen
          name="Cart"
          component={CartScreen}/>
        <Stack.Screen
          name="Progress Bar"
          component={ProgressbarScreen}/>
      </Stack.Navigator>
    </NavigationContainer>
  );
};

export default App;
