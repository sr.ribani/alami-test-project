import React, { useState } from 'react';
import { Button, FlatList, StyleSheet, Text, View } from "react-native"
import { Card, FAB } from 'react-native-paper';

const CartScreen = () => {
    const [list, setList] =  useState([]);

    const addItem = () => {
        setList(arr => [...arr, { key: "Item "+arr.length++ }]);
    };  
    
    const removeItem = key => e => {
        setList(list.filter(item => item.key !== key));
    }

    return (
        <View style={styles.container}>
            <FlatList style={styles.listContainer}
            data={list}
            renderItem={({item}) => <CartItem name={item.key} onRemove={removeItem}/>
            }/>
            <FAB 
            style={styles.fab} 
            onPress={addItem} 
            label="Tambah"/>
        </View>
    )
}

const CartItem = ({name, onRemove}) => {
    return (
        <Card style={styles.cartContainer}>
            <View style={styles.cartInnerContainer}>
                <Text style={styles.cartTitle}>{name}</Text>
                <Button title="Hapus" onPress={onRemove(name)}/>
            </View>
        </Card>
    )
}

export default CartScreen;

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        flexDirection: 'column'
    },
    listContainer: {
        flex: 1
    },
    cartContainer: {
        margin: 5,
        paddingHorizontal: 15,
        paddingVertical: 20
    },
    cartInnerContainer: {
        flexDirection: 'row'
    },
    cartTitle: {
        flex: 1,
    },
    fab: {
        position: 'relative',
        margin: 16,
    },
})