import React, { useState } from 'react';

import { StyleSheet, Text, View, NativeModules } from "react-native"

const { DeviceModule } = NativeModules;

const DeviceIdScreen = () => {
    const [deviceId, setDeviceId] = useState("-")
    DeviceModule.getDeviceId((err, id) => {
        setDeviceId(id)
    })
    return(
        <View style={style.container}>
            <Text>Your device id</Text>
            <Text>{deviceId}</Text>
        </View>
    )
}

export default DeviceIdScreen;

const style = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        flexDirection: 'column'
    }
})