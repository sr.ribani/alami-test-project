import React from 'react';

import { View, Text, StyleSheet, Button } from "react-native";

const HomeScreen = ({navigation}) => {
    return(
        <View style={styles.container}>
            <Button 
                title="Case 1 : Device ID"
                onPress={() => { navigation.navigate("Device Id") }}/>
            <Button 
                title="Case 2 : Cart"
                onPress={() => { navigation.navigate("Cart") }}/>
            <Button 
                title="Case 3 : Progress"
                onPress={() => {navigation.navigate("Progress Bar")}}/>
        </View>
    );
};

export default HomeScreen;

const styles = StyleSheet.create({
    container : {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        flexDirection: 'column',
    },
})