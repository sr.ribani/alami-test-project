import React, { useEffect, useState } from 'react';

import { View, StyleSheet, Animated, TouchableOpacity } from "react-native";
import * as Progress from 'react-native-progress';


const ProgressbarScreen = () => {
    const [count, setCount] = useState(0);
    const [pause, setPause] = useState(false);

    useEffect(() => {
        setTimeout(() => {
            if (count !== 100 && !pause)  {
                setCount((count) => count + 1);
            }
        }, 50);
      });

    return(
        <View style={styles.container}>
            <TouchableOpacity onPressIn={() => setPause(true)} onPressOut={() => setPause(false)}>
                <Progress.Bar progress={count/100}/>
            </TouchableOpacity>
        </View>
    )
}

export default ProgressbarScreen;

const styles = StyleSheet.create({
    container : {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        flexDirection: 'row',
    },
})