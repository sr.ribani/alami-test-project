# Test Project
Untuk menjalankan project ini :
1.  Melalui terminal, clone repo project ini dengan mengikuti command :  `git clone https://gitlab.com/sr.ribani/alami-test-project.git`
2.  Buka direktori project dengan perintah :  `cd alami-test-project`
3.  Jalankan perintah  `npm install`  untuk install seluruh dependency yang dibutuhkan oleh project
4.  Jalankan Simulator IOS atau Android Anda dengan menjalankan perintah   `react-native run-ios`  atau  `react-native run-android`.