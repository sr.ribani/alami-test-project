package com.alamitestproject;

import android.annotation.SuppressLint;
import android.content.Context;
import android.provider.Settings;

import androidx.annotation.NonNull;

import com.facebook.react.bridge.Callback;
import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.bridge.ReactContextBaseJavaModule;
import com.facebook.react.bridge.ReactMethod;

public class DeviceModule extends ReactContextBaseJavaModule {

    private final Context context;

    DeviceModule(ReactApplicationContext context) {
        super(context);

        this.context = context;
    }

    @NonNull
    @Override
    public String getName() {
        return "DeviceModule";
    }

    @SuppressLint("HardwareIds")
    @ReactMethod
    public void getDeviceId(Callback callback) {
        String deviceId = Settings.Secure.getString(context.getContentResolver(),
                Settings.Secure.ANDROID_ID);

        callback.invoke(null, deviceId);

    }
}
