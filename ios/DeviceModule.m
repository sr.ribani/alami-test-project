//
//  DeviceModule.m
//  AlamiTestProject
//
//  Created by M. Ribani on 01/09/22.
//

#import <Foundation/Foundation.h>

#import "React/RCTBridgeModule.h"
@interface
RCT_EXTERN_MODULE(DeviceModule, NSObject)
RCT_EXTERN_METHOD(getDeviceId: (RCTResponseSenderBlock)callback)
@end
