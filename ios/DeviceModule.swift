//
//  DeviceModule.swift
//  AlamiTestProject
//
//  Created by M. Ribani on 01/09/22.
//

import Foundation

@objc(DeviceModule)
class DeviceModule : NSObject {
  
  @objc
  func getDeviceId(_ callback: RCTResponseSenderBlock) -> Void {
    let deviceId = UIDevice.current.identifierForVendor!.uuidString
    callback([NSNull(), deviceId])
  }
  
  @objc
  static func requiresMainQueueSetup() -> Bool {
    return true
  }
}
